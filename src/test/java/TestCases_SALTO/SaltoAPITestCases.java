package TestCases_SALTO;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.fasterxml.jackson.core.JsonProcessingException;

import Activation.ActivationProvisioningNotificationPayload;
import Activation.ActivationResourceProvisioningNotificationPayload;
import Resource.CreationNotificationPostPayload;
import Resource.UpdationNotificationPutPayload;
import ResourceConnectivity.ConnectivityAttributeValueChangeNotificationPayload;
import SaltoUserToken.SaltoUserTokenPayload;

public class SaltoAPITestCases {
	static final Logger log = Logger.getLogger(SaltoAPITestCases.class.getName());
	CreationNotificationPostPayload createNotification;
	SaltoUserTokenPayload saltoUserToken;
	UpdationNotificationPutPayload updateNotification;
	ActivationProvisioningNotificationPayload activationProvisioningNotification;
	ActivationResourceProvisioningNotificationPayload activationResourceProvisioningNotification;
	ConnectivityAttributeValueChangeNotificationPayload connectivityAttributeValueChangeNotification;
	
	@BeforeTest
	public void Init() {
		createNotification = new CreationNotificationPostPayload();
		saltoUserToken = new SaltoUserTokenPayload();
		updateNotification = new UpdationNotificationPutPayload();
		activationProvisioningNotification = new ActivationProvisioningNotificationPayload();
		activationResourceProvisioningNotification = new ActivationResourceProvisioningNotificationPayload();
		connectivityAttributeValueChangeNotification = new ConnectivityAttributeValueChangeNotificationPayload();
	}

	@Test(priority = 0)
	public void Salto_User_GenerateToken_API() throws JsonProcessingException, InterruptedException {
		log.info("########## Start Testing for Salto_User_GenerateToken_API ##########");
		saltoUserToken.SaltoServiceGetAPI();
		log.info("######### End Testing for Salto_User_GenerateToken_API ##########");
	}

	@Test(priority = 1, dependsOnMethods = "Salto_User_GenerateToken_API")
	public void Creation_Notification_API() throws JsonProcessingException, InterruptedException {
		String url = "/resource/access/creationNotification";

		log.info("######### Start Testing for Creation_Notification_API ###########");
		createNotification.CreationNotificationPostAPI(url, SaltoUserTokenPayload.Token);
		log.info("######### End Testing for Creation_Notification_API ##########");
	}

	@Test(priority = 2, dependsOnMethods = "Salto_User_GenerateToken_API")
	public void Updation_Notification_API() throws JsonProcessingException, InterruptedException {
		String url = "/resource/access/updationNotification";

		log.info("################### Start Testing for Updation_Notification_API ###################");
		updateNotification.UpdationNotificationPutAPI(url, SaltoUserTokenPayload.Token);
		log.info("################### End Testing for Updation_Notification_API ###################");
	}
	
	@Test(priority = 3, dependsOnMethods = "Salto_User_GenerateToken_API")
	public void Activation_Provisioning_Notification_Post_API() throws JsonProcessingException, InterruptedException {
		String url = "/activation/service/access/provisioningNotification";

		log.info("################### Start Testing for Activation_Provisioning_Notification_Post_API ###################");
		activationProvisioningNotification.ActivationProvisioningNotificationPostAPI(url, SaltoUserTokenPayload.Token);
		log.info("################### End Testing for Activation_Provisioning_Notification_Post_API ###################");
	}
	
	@Test(priority = 4, dependsOnMethods = "Salto_User_GenerateToken_API")
	public void Activation_Provisioning_Notification_Put_API() throws JsonProcessingException, InterruptedException {
		String url = "/activation/service/access/provisioningNotification";

		log.info("################### Start Testing for Activation_Provisioning_Notification_Put_API ###################");
		activationProvisioningNotification.ActivationProvisioningNotificationPutAPI(url, SaltoUserTokenPayload.Token);
		log.info("################### End Testing for Activation_Provisioning_Notification_Put_API ###################");
	}
	
	@Test(priority = 5, dependsOnMethods = "Salto_User_GenerateToken_API")
	public void Activation_Resource_Provisioning_Notification_Post_API() throws JsonProcessingException, InterruptedException {
		String url = "/activation/service/access/resourceProvisioningNotification";

		log.info("################### Start Testing for Activation_Resource_Provisioning_Notification_Post_API ###################");
		activationResourceProvisioningNotification.ActivationResourceProvisioningNotificationPayloadPostAPI(url, SaltoUserTokenPayload.Token);
		log.info("################### End Testing for Activation_Resource_Provisioning_Notification_Post_API ###################");
	}
	
	@Test(priority = 6, dependsOnMethods = "Salto_User_GenerateToken_API")
	public void Activation_Resource_Provisioning_Notification_Put_API() throws JsonProcessingException, InterruptedException {
		String url = "/activation/service/access/resourceProvisioningNotification";

		log.info("################### Start Testing for Activation_Resource_Provisioning_Notification_Put_API ###################");
		activationResourceProvisioningNotification.ActivationResourceProvisioningNotificationPayloadPutAPI(url, SaltoUserTokenPayload.Token);
		log.info("################### End Testing for Activation_Resource_Provisioning_Notification_Put_API ###################");
	}
	
	@Test(priority = 7, dependsOnMethods = "Salto_User_GenerateToken_API")
	public void Connectivity_Attribute_Value_Change_Notification_Post_API() throws JsonProcessingException, InterruptedException {
		String url = "/connectivity/access/attributeValueChangeNotification";

		log.info("################### Start Testing for Connectivity_Attribute_Value_Change_Notification_Post_API ###################");
		connectivityAttributeValueChangeNotification.ConnectivityAttributeValueChangeNotificationPostAPI(url, SaltoUserTokenPayload.Token);
		log.info("################### End Testing for Connectivity_Attribute_Value_Change_Notification_Post_API ###################");
	}
}
