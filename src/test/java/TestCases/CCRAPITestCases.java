package TestCases;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.fasterxml.jackson.core.JsonProcessingException;

import Activation.ActivationServicePayload;
import Activation.ActivationServiceResourcePayload;
import Individual.ResourcePartyTypePayload;
import PartyAccount.PartyAccountPayload;
import Resource.ResourceInfoPayload;
import Resource.ResourceInventoryPayload;
import Resource.ResourcePayload;
import Resource.ResourceTypeBackbonePayload;
import Resource.ResourceTypePayload;
import ResourceConnectivity.ResourceConnectivityPayload;
import ResourceConnectivity.ResourceConnectivityTypePayload;
/*import TestBase.testbase;*/
import UserToken.UserTokenPayload;

public class CCRAPITestCases  {

	static final Logger log = Logger.getLogger(CCRAPITestCases.class.getName());
	UserTokenPayload userToken;
	ResourceConnectivityPayload resourceConnectivity;
	PartyAccountPayload partyAccount;
	ResourcePayload resource;
	ActivationServicePayload activationService;
	ResourcePartyTypePayload resourcePartyType;
	ResourceInfoPayload resourceInfo;
	ResourceTypePayload resourceType;
	ResourceConnectivityTypePayload resourceConnectivityType;
	ResourceTypeBackbonePayload resourceTypeBackbone;
	ResourceInventoryPayload resourceInventory;
	ActivationServiceResourcePayload activationServiceResource;
	
	@BeforeTest
	public void Init() {
		userToken = new UserTokenPayload();
		resourceConnectivity = new ResourceConnectivityPayload();
		partyAccount = new PartyAccountPayload();
		resource = new ResourcePayload();
		activationService = new ActivationServicePayload();
		resourcePartyType = new ResourcePartyTypePayload();
		resourceInfo = new ResourceInfoPayload();
		resourceType = new ResourceTypePayload();
		resourceConnectivityType = new ResourceConnectivityTypePayload();
		resourceTypeBackbone = new ResourceTypeBackbonePayload();
		resourceInventory = new ResourceInventoryPayload();
		activationServiceResource = new ActivationServiceResourcePayload();
	}

	@Test(priority = 0)
	public void User_GenerateToken_API() throws JsonProcessingException, InterruptedException {
		log.info("################### Start Testing for User_GenerateToken_API ###################");
		userToken.ServiceGetAPI();
		log.info("################### End Testing for User_GenerateToken_API ###################");
	}

	@Test(priority = 1, dependsOnMethods = "User_GenerateToken_API")
	public void Resource_Connectivity_API() throws JsonProcessingException, InterruptedException {

		String url = "/resource/connectivity?";
		String urldata = "resourceId=SINSIN101";

		log.info("################### Start Testing for Resource_Connectivity_API ###################");
		resourceConnectivity.ResourceConnectivityGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Resource_Connectivity_API ###################");
	}

	@Test(priority = 2, dependsOnMethods = "User_GenerateToken_API")
	public void Party_Account_API() throws JsonProcessingException, InterruptedException {

		String url = "/partyAccount?";
		String urldata = "id=1";

		log.info("################### Start Testing for Party_Account_API ###################");
		partyAccount.PartyGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Party_Account_API ###################");
	}

	@Test(priority = 3, dependsOnMethods = "User_GenerateToken_API")
	public void Resource_API() throws JsonProcessingException, InterruptedException {

		String url = "/resource/";
		String urldata = "SINSIN101";

		log.info("################### Start Testing for Resource_API ###################");
		resource.ResourceGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Resource_API ###################");
	}

	@Test(priority = 4, dependsOnMethods = "User_GenerateToken_API")
	public void Activation_Service_API() throws JsonProcessingException, InterruptedException {
		String url = "/activation/service?";
		String urldata = "serviceId=1";

		log.info("################### Start Testing for Activation_Service_API ###################");
		activationService.ActivationServiceGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Activation_Service_API ###################");

	}

	@Test(priority = 5, dependsOnMethods = "User_GenerateToken_API")
	public void Resource_Party_Type_API() throws JsonProcessingException, InterruptedException {
		String url = "/resource/partyType?";
		String urldata = "limit=100&offset=0&partyId=1&type=CRM_CUSTOMER";

		log.info("################### Start Testing for Resource_Party_Type_API ###################");
		resourcePartyType.ResourcePartyTypeAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Resource_Party_Type_API ###################");

	}

	@Test(priority = 6, dependsOnMethods = "User_GenerateToken_API")
	public void Resource_Info_API() throws JsonProcessingException, InterruptedException {
		String url = "/resource/info?";
		String urldata = "limit=1000&offset=0";

		log.info("################### Start Testing for Resource_Info_API ###################");
		resourceInfo.ResourceInfoGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Resource_Info_API ###################");

	}

	@Test(priority = 7, dependsOnMethods = "User_GenerateToken_API")
	public void Resource_Type_API() throws JsonProcessingException, InterruptedException {
		String url = "/resource/type?";
		String urldata = "limit=100&offset=0&type=ACCESS";

		log.info("################### Start Testing for Resource_Type_API ###################");
		resourceType.ResourceTypeGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Resource_Type_API ###################");

	}

	@Test(priority = 8, dependsOnMethods = "User_GenerateToken_API")
	public void Resource_Connectivity_Type_API() throws JsonProcessingException, InterruptedException {
		String url = "/resource/connectivityType?";
		String urldata = "limit=100&offset=0&type=ACCESS";

		log.info("################### Start Testing for Resource_Connectivity_Type_API ###################");
		resourceConnectivityType.ResourceConnectivityTypeGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Resource_Connectivity_Type_API ###################");

	}
	
	@Test(priority = 9, dependsOnMethods = "User_GenerateToken_API")
	public void Resource_Type_Backbone_API() throws JsonProcessingException, InterruptedException {
		String url = "/resource/type/backbone?";
		String urldata = "limit=100&offset=0&resourceId=SINSIN100";

		log.info("################### Start Testing for Resource_Type_Backbone_API ###################");
		resourceTypeBackbone.ResourceTypeBackboneGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Resource_Type_Backbone_API ###################");

	}
	
	@Test(priority = 10, dependsOnMethods = "User_GenerateToken_API")
	public void Resource_Inventory_API() throws JsonProcessingException, InterruptedException {
		String url = "/resourceInventory?";
		String urldata = "limit=1000&offset=0";

		log.info("################### Start Testing for Resource_Inventory_API ###################");
		resourceInventory.ResourceInventoryGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Resource_Inventory_API ###################");

	}
	
	@Test(priority = 11, dependsOnMethods = "User_GenerateToken_API")
	public void Activation_Service_Resource_API() throws JsonProcessingException, InterruptedException {
		String url = "/activation/service/resourceId/";
		String urldata = "SINSIN100";

		log.info("################### Start Testing for Activation_Service_Resource_API ###################");
		activationServiceResource.ActivationServiceResourceGetAPI(url, urldata, UserTokenPayload.Token);
		log.info("################### End Testing for Activation_Service_Resource_API ###################");

	}

}
