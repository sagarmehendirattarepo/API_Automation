package PartyAccount;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;
import TestBase.testbase;
import UserToken.UserTokenPayload;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import junit.framework.Assert;

public class PartyAccountPayload extends testbase{
	
	public static String Token;
	public static final Logger log = Logger.getLogger(PartyAccountPayload.class.getName());


	public void PartyGetAPI(String url, String urldata, String Token) throws JsonProcessingException, InterruptedException{

		String ApiURL = Env + url + urldata;
		System.out.println("Get ApiURL : " + ApiURL);
		//System.out.println("Print token: " + Token);
		RestAssured.baseURI = ApiURL;
		RestAssured.useRelaxedHTTPSValidation();
		Response Getresponse = RestAssured.given().baseUri(ApiURL).header("accept", "application/hal+json")
				.header("authorizationToken", Token).get(ApiURL);

		@SuppressWarnings("rawtypes")
		ResponseBody body = Getresponse.getBody();
		log.info("Response body :" + body.asString());
		System.out.println("Response body :" + body.asString());
		int StatusCode = Getresponse.getStatusCode();
		log.info("Status code of Get API is : " + StatusCode);
		Assert.assertEquals(200, StatusCode);
	
	}
	///// This main method is used only for testing////
	public static void main(String args[]) throws InterruptedException, JsonProcessingException {
		String url="/partyAccount?";
		String urldata="id=1";

		UserTokenPayload userToken2 = new UserTokenPayload();
		userToken2.ServiceGetAPI();

		PartyAccountPayload partyAccount = new PartyAccountPayload();
		partyAccount.PartyGetAPI(url, urldata,UserTokenPayload.Token);
	}
}
