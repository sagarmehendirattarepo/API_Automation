package ExtentReport;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {

	public static ExtentReports extent;

    public synchronized static ExtentReports getReporter(){
        if(extent == null){
            //Set HTML reporting file location
            String workingDir = System.getProperty("user.dir");
            extent = new ExtentReports(workingDir+"\\src\\main\\java\\reports\\APIAutomationReport.html", true);
            extent.addSystemInfo("User Name", "CCR");
    		//extent.addSystemInfo("Environment", Env);
    		//extent.addSystemInfo("Browser Version", "60.8");
        }
        return extent;

    }
}