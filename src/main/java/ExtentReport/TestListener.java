package ExtentReport;

import org.testng.ITestListener;

import com.relevantcodes.extentreports.LogStatus;

import TestBase.testbase;

import java.util.HashMap;
import java.util.Map;

//import org.openqa.selenium.OutputType;
//import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestContext;
import org.testng.ITestResult;


public class TestListener extends testbase implements ITestListener {
//	FinalPayload service = new FinalPayload();
//	AddAttachmentInJira addattachment = new AddAttachmentInJira();
//	FinalPayloadComponent finalpayloadcomponent =new FinalPayloadComponent();
	public static Map<String, String> FailedTestMap = new HashMap<String, String>();
	
	 private static String getTestMethodName(ITestResult iTestResult) {
	        return iTestResult.getMethod().getConstructorOrMethod().getName();
	    }
	    
	    //Before starting all tests, below method runs.
	   // @Override
	    public void onStart(ITestContext iTestContext) {
	        System.out.println("I am in onStart method " + iTestContext.getName());
	       // iTestContext.setAttribute("WebDriver", this.driver);
	        
	        
	    }
	 
	    //After ending all tests, below method runs.
	   // @Override
	    public void onFinish(ITestContext iTestContext) {
	        System.out.println("I am in onFinish method " + iTestContext.getName());
	        //Do tier down operations for extentreports reporting!
	        ExtentTestManager.endTest();
	        ExtentManager.getReporter().flush();
	    }
	 
	  //  @Override
	    public void onTestStart(ITestResult iTestResult) {
	        System.out.println("I am in onTestStart method " +  getTestMethodName(iTestResult) + " start");
	        //Start operation for extentreports.
	        ExtentTestManager.startTest(iTestResult.getMethod().getMethodName(),"");
	    }
	 
	  //  @Override
	    public void onTestSuccess(ITestResult iTestResult) {
	        System.out.println("I am in onTestSuccess method " +  getTestMethodName(iTestResult) + " succeed");
	        //Extentreports log operation for passed tests.
	        ExtentTestManager.getTest().log(LogStatus.PASS, getTestMethodName(iTestResult)+ " Test passed and response received");
	    }
	 
	  //  @Override
	    public void onTestFailure(ITestResult iTestResult) {
	        System.out.println("I am in onTestFailure method " +  getTestMethodName(iTestResult) + " failed");
	     //   ExtentTestManager.getTest().log(LogStatus.FAIL, getTestMethodName(iTestResult)+ " Test failed");
	        //Get driver from BaseTest and assign to local webdriver variable.
	       // Object testClass = iTestResult.getInstance();
	       // driver = ((TestBase) testClass).driver;
	 
	        //Take base64Screenshot screenshot.
	     //   String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)driver).
	      //          getScreenshotAs(OutputType.BASE64);
	        
	        ExtentTestManager.getTest().log(LogStatus.FAIL, getTestMethodName(iTestResult)+ " Test failed");
	 
	        //Extentreports log and screenshot operations for failed tests.
	      //  ExtentTestManager.getTest().log(LogStatus.FAIL,"Test Failed",ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));
	      //  String imagePath = captureScreen(iTestResult.getName());
		/*
		 * try { //
		 * service.createJiraBugAPI("Bug","PBM",iTestResult.getThrowable().toString(),
		 * getTestMethodName(iTestResult)+ " Test failed","FLIPE-4764"); } catch
		 * (JsonProcessingException e) {
		 * System.out.println("Excetion while creation of Jira bug"); }
		 * 
		 * try { // finalpayloadcomponent.ServicePayloadComponent("Test_Automation"); }
		 * catch (JsonProcessingException e2) {
		 * System.out.println("Exception while update component"); }
		 * 
		 * 
		 * try { // FailedTestMap.put(getTestMethodName(iTestResult)+ " Test failed",
		 * service.JiraBugID); //
		 * System.out.println("Test Case Name and Jira Bug ID is : "+getTestMethodName(
		 * iTestResult)+ " Test failed and " +service.JiraBugID); } catch (Exception e)
		 * { System.out.println("No data Stored in MAP for Failed TestCase"); }
		 */
	    }
	 
	  //  @Override
	    public void onTestSkipped(ITestResult iTestResult) {
	        System.out.println("I am in onTestSkipped method "+  getTestMethodName(iTestResult) + " skipped");
	        //Extentreports log operation for skipped tests.
	        ExtentTestManager.getTest().log(LogStatus.SKIP,getTestMethodName(iTestResult)+ " Test Skipped");
	    }
	 
	 //   @Override
	    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
	        System.out.println("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
	    }

	    
	   
}
