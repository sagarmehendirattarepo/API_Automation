package Util;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
//import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class Browser {
	
	public  DesiredCapabilities capabilities;
	public  FirefoxProfile profile;
	
	@SuppressWarnings("deprecation")
	public  WebDriver CallingWebBrowserDriver(String BrowserName) throws MalformedURLException {
		
		WebDriver driver = null;
		
		if(BrowserName.equals("Chrome")) {			
			String DriverPath=System.getProperty("user.dir")+"\\chromedriver.exe";
			System.setProperty("webdriver.chrome.driver",DriverPath);
			ChromeOptions options = new ChromeOptions();
			//options.addArguments("--incognito");
			options.addArguments("test-type");
			options.addArguments("--disable-popup-blocking");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);			
			driver=new ChromeDriver(capabilities);
			
			/*URL url=new URL("http://localhost:4444/wd/hub");
			ChromeOptions options = new ChromeOptions();
			options.addArguments("test-type");
			options.addArguments("--disable-popup-blocking");
			options.addArguments("--ignore-certificate-errors");			
			DesiredCapabilities cap=DesiredCapabilities.chrome();
			cap.setCapability (CapabilityType.ACCEPT_SSL_CERTS, true);
			cap.setCapability(ChromeOptions.CAPABILITY, options);	
			driver = new RemoteWebDriver(url,cap);*/
			
		}else if(BrowserName.equals("Firefox")) {
			
			capabilities = DesiredCapabilities.firefox();
			profile = new FirefoxProfile();	
			profile.setPreference("intl.accept_languages","en");
			//profile.setPreference("browser.privatebrowsing.autostart", true);
			capabilities.setCapability(FirefoxDriver.PROFILE, profile);	
			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"/geckodriver.exe");			
			//driver = new FirefoxDriver(capabilities);
			capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
			
			driver = new FirefoxDriver(capabilities);
			
			/*capabilities = DesiredCapabilities.firefox();
			profile = new FirefoxProfile();	
			profile.setPreference("intl.accept_languages","en");
			profile.setPreference("security.mixed_content.block_active_content","false");
			profile.setPreference("security.mixed_content.block_display_content","true");
			//profile.setPreference("browser.privatebrowsing.autostart", true);
			capabilities.setCapability(FirefoxDriver.PROFILE, profile);	
			System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir")+"/geckodriver.exe");			
			driver = new FirefoxDriver(capabilities);*/
			
			/*URL url=new URL("http://localhost:4444/wd/hub");
			DesiredCapabilities cap=DesiredCapabilities.firefox();
			driver = new RemoteWebDriver(url,cap);*/
			
		}else if(BrowserName.equals("IE")) {
			capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability("intl.accept_languages","en");
			capabilities.setCapability(InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);			
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			System.setProperty("webdriver.ie.driver",System.getProperty("user.dir")+"/IEDriverServer_32.exe");
			driver = new InternetExplorerDriver(capabilities);
		}else if(BrowserName.equals("Microsoft Edge")) {
			
			String DriverPath=System.getProperty("user.dir")+"\\msedgedriver.exe";
			System.setProperty("webdriver.edge.driver", DriverPath);
			driver = new EdgeDriver();
		}
		return driver;
	}

}
