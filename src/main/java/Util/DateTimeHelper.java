package Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

public class DateTimeHelper {
	
	public static final Logger log =Logger.getLogger(DateTimeHelper.class.getName());
	
	public static String DiffSeconds(String Datetime1,String Datetime2 ) throws ParseException
	{

			String date1 = Datetime1;
			String date2 = Datetime2;

			//HH converts hour in 24 hours format (0-23), day calculation
			SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

			Date d1 = null;
			Date d2 = null;

			
				d1 = format.parse(date1);
				d2 = format.parse(date2);

				//in milliseconds
				long diff = d2.getTime() - d1.getTime();

				long diffSeconds = diff / 1000 % 60;
				long diffMinutes = diff / (60 * 1000) % 60;
				long diffHours = diff / (60 * 60 * 1000) % 24;
				long diffDays = diff / (24 * 60 * 60 * 1000);

			//	System.out.println("############# Diff between DateTime ###############");
			//	System.out.println(diffDays + " days, ");
			//	System.out.println(diffHours + " hours, ");
			//	System.out.println(diffMinutes + " minutes, ");
			//	System.out.println(diffSeconds + " seconds.");
				
				String  MinSecDiff = diffMinutes+" min "+diffSeconds+" sec";
				System.out.println("Time difference to Start and End Test : "+MinSecDiff);
			//	System.out.println("###################################################");
				
				return MinSecDiff;
	}
	
	public static String ActualEndTime(String TestStartTime,String TestEndTime) throws ParseException
	{
		String date1 = "TestStartTime";	
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				Date d1 = null;
			 d1 = format.parse(date1);
		  
	    //  Date d1 = new Date();
	     
	      
			Calendar cl = Calendar. getInstance();
	      
	      cl.setTime(d1);	 
	      System.out.println("today is " + d1.toString());
	      
	     /* cl. add(Calendar.MONTH, 1);
	      System.out.println("date after a month will be " + cl.getTime().toString() );*/
	      String TimeFromTest = TestEndTime;
	      String[] splitStr = TimeFromTest.trim().split(":");
	      
	      String shour = splitStr[0];
	      int hour = Integer.parseInt(shour);	
	      
	      String sminute = splitStr[1];
	      int minute = Integer.parseInt(sminute);
	      
	      String ssecond = splitStr[2];
	      int second = Integer.parseInt(ssecond);	
	      
	      
	      
	      cl. add(Calendar.HOUR, hour);	     
	      System.out.println("date after "+shour+" hrs will be " +  format.format(cl.getTime()).toString() );
	      
	      cl. add(Calendar.MINUTE, minute);
	      System.out.println("date after "+sminute+" min will be " +  format.format(cl.getTime()).toString() );
	      
	      cl. add(Calendar.SECOND, second);
	      System.out.println("date after "+ssecond+" sec will be " +  format.format(cl.getTime()).toString() );
	      
	      /*cl. add(Calendar.YEAR, 3);
	      System.out.println("date after 3 years will be " +  format.format(cl.getTime()).toString() );*/
	      String ActualEndTestDateTime = format.format(cl.getTime()).toString();
	      
	      System.out.println("Actual End time after add DateTime is : "+ActualEndTestDateTime);
	      
	      return ActualEndTestDateTime;
	}
	
	public long DateDiff_ForJOB(String Datetime1,String Datetime2 ) throws ParseException
	{

			String date1 = Datetime1;
			String date2 = Datetime2;

			//HH converts hour in 24 hours format (0-23), day calculation
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

			Date d1 = null;
			Date d2 = null;

			
				d1 = format.parse(date1);
				d2 = format.parse(date2);

				//in milliseconds
				long diff = d2.getTime() - d1.getTime();

				long diffSeconds = diff / 1000 % 60;
				long diffMinutes = diff / (60 * 1000) % 60;
				long diffHours = diff / (60 * 60 * 1000) % 24;
				long diffDays = diff / (24 * 60 * 60 * 1000);

				System.out.println("############# Diff between DateTime ###############");
				System.out.println(diffDays + " days, ");
				System.out.println(diffHours + " hours, ");
				System.out.println(diffMinutes + " minutes, ");
				System.out.println(diffSeconds + " seconds.");
				System.out.println("###################################################");
				
				return diffDays;
	}



}
