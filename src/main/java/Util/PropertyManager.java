package Util;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertyManager {
	
	private  PropertyManager instance;
    private  final Object lock = new Object();
    private  String propertyFilePath = System.getProperty("user.dir")+"\\src\\main\\java\\Config\\configuration.properties";
    private  String Browser;
    private  String url;
    private  String Username;
    private  String Password; 
    private  String ccTestDataFileName; 
    private  String apiurl; 
    private String env;
  
    
  //Create a Singleton instance. We need only one instance of Property Manager.
    public  PropertyManager getInstance () {
        if (instance == null) {
            synchronized (lock) {
                instance = new PropertyManager();
                instance.loadData();
            }
        }
        return instance;
    }


    
	private void loadData() {
		
        Properties prop = new Properties();
 
        try {
            prop.load(new FileInputStream(propertyFilePath));            
        } catch (IOException e) {
            System.out.println("Configuration properties file cannot be found");
            System.out.println("Path is"+propertyFilePath);
        }
 
        Browser=prop.getProperty("Browser");
        url = prop.getProperty("url");
        Username = prop.getProperty("Username");
        Password = prop.getProperty("Password");
        ccTestDataFileName=prop.getProperty("ccrTestdataFileName");
        apiurl=prop.getProperty("apiurl");
        env= prop.getProperty("Environment");
        
    }
	
	public String getBrowser() {
		return Browser;
	}
	
	public String getURL () {
	      return url;
	}
	 
	public String getEnv() {
		return env;
	}

	public String getUsername () {
	      return Username;
	 }
	 
	public String getPassword () {
	      return Password;
	 }
	
	public String ccrTestdataFileName () {
	      return ccTestDataFileName;
	 }
	
	public String apiurl () {
	      return apiurl;
	 }
	
	
	
	}


