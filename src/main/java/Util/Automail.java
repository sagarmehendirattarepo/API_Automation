package Util;

//import java.awt.image.BufferedImage;
import java.io.File;
//import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
//import javax.imageio.ImageIO;
import javax.mail.Authenticator;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
//import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.edge.EdgeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;
//import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;

import org.testng.annotations.Test;

import com.google.common.io.Files;

public class Automail {

	private static WebDriver driver;
	private static String Env;
	// private static Properties prop1;

	private static String ccrECMSURL;

	@Test
	public void Gen_Reports()
			throws InterruptedException, InvalidFormatException, IOException, AddressException, MessagingException, ParseException {

		DateFormat dateFormat1 = new SimpleDateFormat("dd/MMM/yyyy");
		Date date1 = new Date();
		System.out.println(dateFormat1.format(date1));
		Browser Browser = new Browser();
		
		System.setProperty("webdriver.edge.driver",System.getProperty("user.dir")+"\\msedgedriver.exe");
        driver = new EdgeDriver();

		//driver = Browser.CallingWebBrowserDriver("Microsoft Edge");
		Thread.sleep(3000);

//Taking Screenshots

//driver.get("file:///C:/Users/XMJF0501/eclipse-workspace/MitroTest/test-output/MitroAutomationTestResults.html");
		String ReportPath = System.getProperty("user.dir")
				+ "/src/main/java/reports/APIAutomationReport.html";
		System.out.println();
		driver.get("file:///" + ReportPath);

		driver.findElement(By.xpath(".//*[@id='slide-out']//li[2]//a")).click();
		Thread.sleep(1000);

		WebElement ele3 = driver.findElement(By.xpath(".//*[@id='dashboard-view']"));
		Thread.sleep(1000);

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", ele3);
//WebElement ele4 = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(".//*[@id='dashboard-view']")));
		File screen2 = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		System.out.println("table screenshot done");
		String currentDir1 = System.getProperty("user.dir");
		Files.copy(screen2, new File(currentDir1 + "/ReportImage/" + "piechart" + ".jpg"));
		System.out.println("copied");
		Thread.sleep(2000);
		System.out.println("Start mail");

		String to = "xyz@gmail.com";		
		String from = "xyz@gmail.com";
		final String username = "xyz@gmail.com";
		final String password = "xyz@123";
		String host = "exchange-eme6.itn.ftgroup";

//String to = "tcoe_india@list2.orange.com";
		/*
		 * String to =
		 * "sdsimscindia@list2.orange.com,OBS.Agile.Evolution@soprasteria.com,aisrm.clarify.team@orange.com";
		 * String from = "tcoe_india@list2.orange.com"; final String username =
		 * "tcoe_india@list2.orange.com"; final String password = ""; String host =
		 * "mx-us.equant.com";
		 */

//######################################################################
		PropertyManager PropertyManager = new PropertyManager();
		Env = "UAT";
		ccrECMSURL = PropertyManager.getInstance().getURL();

		String subject = "[" + Env + "][" + dateFormat1.format(date1) + "] CCR Circuit API Sanity Automated Report";
//String subject = "[QA]"+dateFormat1.format(date1)+"] Flip Sanity Automated Report";
		StringBuffer body = new StringBuffer("<html><body><p><font color=\"blue\">Hello  All,<br><br>");
		body.append("Please find the below Automated CCR Circuit API " + Env + " Sanity report:<br><br>");
//body.append("Please find the below Automated Flip QA Sanity report:<br><br>");
		;
//body.append("Please find below the automated Mitro Sanity report:<br><br>");
		body.append("<img style=\"border:10px solid black\" src=\"cid:image2\" width=\"90%\" height=\"60%\"/><br>");
//body.append("<img style=\"border:10px solid black\" src=\"cid:image1\" width=\"120%\" height=\"75%\"/><br>" );

		driver.findElement(By.xpath(".//*[@id='slide-out']//li[1]//a")).click();
		Thread.sleep(5000);

		List<WebElement> TestCount = driver.findElements(By.xpath("//span[@class='test-name']"));
		System.out.println("No of Test :" + TestCount.size());

		body.append("<table>");
		body.append("<style>");
		body.append("table,th,td{border:1px dotted black;border-collapse:collapse;padding:5px;font-size:15px}");
		body.append("th{background:purple}");
		body.append("</style>");

		body.append("<tr>");
		body.append("<th>Status</th>");
		body.append("<th>TimeStamp</th>");
		body.append("<th>TestCaseExecuted</th>");
		body.append("<th>ResponseTime</th>");
		body.append("</tr>");

		//new code
		
		for (int i = 0; i < TestCount.size(); i++) {
			String TestName = TestCount.get(i).getText().toString().trim();
			System.out.println("Test Name is : " + TestName);
			
			JavascriptExecutor je = (JavascriptExecutor) driver;
			je.executeScript("arguments[0].scrollIntoView(true);",TestCount.get(i));
			Thread.sleep(2000);
			TestCount.get(i).click();
			Thread.sleep(3000);

			int TestCountInside = driver.findElements(By.xpath(
					"//div[@class='card-panel details-view']/div[@class='details-container']/div[@class='test-body']/div[@class='test-steps']/table[@class='bordered table-results']/tbody/tr"))
					.size();
			System.out.println("Total Test Case Count " + TestCountInside);

			String Xpath1 = "//div[@class='card-panel details-view']/div[@class='details-container']/div[@class='test-body']/div[@class='test-steps']/table[@class='bordered table-results']/tbody/tr[";
			String Xpath2 = "]/td[1]";

			String Xpath3 = "//div[@class='card-panel details-view']/div[@class='details-container']/div[@class='test-body']/div[@class='test-steps']/table[@class='bordered table-results']/tbody/tr[";
			String Xpath4 = "]/td[2]";

			String Xpath5 = "//div[@class='card-panel details-view']/div[@class='details-container']/div[@class='test-body']/div[@class='test-steps']/table[@class='bordered table-results']/tbody/tr[";
			String Xpath6 = "]/td[3]";

			String TestStartTime = driver.findElement(By.xpath(
					"//div[@class='card-panel details-view']/div[@class='details-container']/div[@class='test-body']/div[@class='test-info']/span[@title='Test started time']"))
					.getText().toString().trim();
			System.out.println("Test Start time is : " + TestStartTime);

			String[] splitStr = TestStartTime.trim().split("\\s+");

			System.out.println("Actual Test Date is : " + splitStr[0]);
			System.out.println("Actual Test Start Time is : " + splitStr[1]);
			String ActualTestStartTime = splitStr[1];

			for (int t = 1; t <= TestCountInside; t++) {
				String Status = Xpath1 + t + Xpath2;
				String StatusTest = driver.findElement(By.xpath(Status)).getAttribute("title").toString();

				String TestTime = Xpath3 + t + Xpath4;
				String TestTimeTest = driver.findElement(By.xpath(TestTime)).getText().toString();

				String TestNameInside = Xpath5 + t + Xpath6;
				String TestCaseName = driver.findElement(By.xpath(TestNameInside)).getText().toString();

				System.out.println("Actual Test End Time is : " + TestTimeTest);

				// String
				// ActualTestStartTime=DateTimeHelper.ActualEndTime(TestStartTime,TestTimeTest);

				String ResponseTimeTest = DateTimeHelper.DiffSeconds(ActualTestStartTime, TestTimeTest);
				
				String SkipTime = "0 min 0 sec";
				String MinusTime = "0 min 22 sec";
				body.append("<tr>");
				if (StatusTest.equalsIgnoreCase("pass")) {
					body.append("<p><font color=\"green\">");
					body.append("<td>" + StatusTest + "</td>");
					body.append("<td>" + TestTimeTest + "</td>");
					// body.append("<td>"+TestName+"</td>");
					body.append("<td>" + TestCaseName + "</td>");

					if (ResponseTimeTest.startsWith("-")) {
						body.append("<td>" + MinusTime + "</td>");
					} else {
						body.append("<td>" + ResponseTimeTest + "</td>");
					}
					body.append("</p></font>");
				} else if (StatusTest.equalsIgnoreCase("fail")) {
					body.append("<p><font color=\"red\">");
					body.append("<td>" + StatusTest + "</td>");
					body.append("<td>" + TestTimeTest + "</td>");
					// body.append("<td>"+TestName+"</td>");
					body.append("<td>" + TestCaseName + "</td>");
					if (ResponseTimeTest.startsWith("-")) {
						body.append("<td>" + MinusTime + "</td>");
					} else {
						body.append("<td>" + ResponseTimeTest + "</td>");
					}
					body.append("</p></font>");
				}

				else if (StatusTest.equalsIgnoreCase("skip")) {
					body.append("<p><font color=\"blue\">");
					body.append("<td>" + StatusTest + "</td>");
					body.append("<td>" + TestTimeTest + "</td>");
					// body.append("<td>"+TestName+"</td>");
					body.append("<td>" + TestCaseName + "</td>");
					body.append("<td>" + SkipTime + "</td>");
					body.append("</p></font>");
				}
				body.append("</tr>");

			}

		}
		
		//old code
		

		body.append("</table>");

		body.append("<p><font color=\"blue\">This is automatic report.</font></p><br><br><br>");
		body.append("</body></html>");

//################################################################################################

// inline images
		Map<String, String> inlineImages = new HashMap<String, String>();
// inlineImages.put("image1", "C:\\Scripts\\CCR_AutomationSuite\\screenshot\\config.jpg");
// inlineImages.put("image2", "C:\\Scripts\\CCR_AutomationSuite\\screenshot\\cases.jpg");

//inlineImages.put("image1", System.getProperty("user.dir")+"/ReportImage/" + "dashboard" + ".jpg");
		inlineImages.put("image2", System.getProperty("user.dir") + "/ReportImage/" + "piechart" + ".jpg");
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		//props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", host);
		props.put("mail.debug", "true");
		props.put("mail.smtp.port", 25);
		props.put("mail.smtp.ssl.trust", host);
		props.put("mail.user", username);
		props.put("mail.password", password);

// creates a new session with an authenticator
		Authenticator auth = new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		};
		Session session = Session.getInstance(props, auth);
// creates a new e-mail message
		Message msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress(from));
		msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
		msg.setSubject(subject);
		msg.setSentDate(new Date());

// creates message part
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setContent(body.toString(), "text/html");
		Multipart multipart = new MimeMultipart();
		multipart.addBodyPart(messageBodyPart);

		MimeBodyPart messageBodyPart2 = new MimeBodyPart();
		String filename = System.getProperty("user.dir")
				+ "/src/main/java/reports/APIAutomationReport.html";
		DataSource source = new FileDataSource(filename);
		messageBodyPart2.setDataHandler(new DataHandler(source));
		messageBodyPart2.setFileName("Report.html");
		multipart.addBodyPart(messageBodyPart2);

// adds inline image attachments
		if (inlineImages != null && inlineImages.size() > 0) {
			Set<String> setImageID = inlineImages.keySet();

			for (String contentId : setImageID) {
				MimeBodyPart imagePart = new MimeBodyPart();
				imagePart.setHeader("Content-ID", "<" + contentId + ">");
				imagePart.setDisposition(MimeBodyPart.INLINE);

				String imageFilePath = inlineImages.get(contentId);
				try {
					imagePart.attachFile(imageFilePath);
				} catch (IOException ex) {
					ex.printStackTrace();
				}

				multipart.addBodyPart(imagePart);
			}
		}
		msg.setContent(multipart);
		Transport.send(msg);

	}

	@AfterTest
	public void DriverQuitReport() {
		driver.quit();
	}

}
