package Activation;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import SaltoUserToken.SaltoUserTokenPayload;
import TestBase.testbase;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import junit.framework.Assert;

public class ActivationResourceProvisioningNotificationPayload extends testbase {
	public static final Logger log = Logger
			.getLogger(ActivationResourceProvisioningNotificationPayload.class.getName());

	////////////////////////////// Method 1 ::::: POST//////////////////////////////////////////
	public void ActivationResourceProvisioningNotificationPayloadPostAPI(String url, String Token)
			throws InterruptedException, JsonProcessingException {
		String ApiURL = EnvForSalto + url;
		System.out.println("Get ApiURL : " + ApiURL);
		log.info(ApiURL);
		RestAssured.baseURI = ApiURL;
		RestAssured.useRelaxedHTTPSValidation();
		String value1 = "{\r\n    \"resource\": \"CA0E5DEDAE\",\r\n    \"cctRequest\": {\r\n        \"scheduledActionDate\": \"31-JUL-21\",\r\n        \"acknowledgementDate\": \"13-JUL-21\",\r\n        \"sourceCiwoId\": \"9000000046\",\r\n        \"status\": \"AK\",\r\n        \"requestedActionDate\": \"31-JUL-21\",\r\n        \"actionDate\": \"14-JUL-21\",\r\n        \"actionType\": \"INSTALL\",\r\n        \"createdDate\": \"14-JUL-21\",\r\n        \"owner\": {\r\n            \"actionOwner\": \"salto\"\r\n        },\r\n        \"createdBy\": \"OWJC7765\",\r\n        \"dateOfAction\": \"\",\r\n        \"user\": \"salto\",\r\n        \"sourceSioId\": \"9000003450\"\r\n    }\r\n}";

		Response Getresponse = RestAssured.given().header("accept", "application/hal+json")
				.header("Content-Type", "application/json").header("authorizationToken", Token).body(value1)
				.post(ApiURL);
		@SuppressWarnings("rawtypes")
		ResponseBody body = Getresponse.getBody();
		System.out.println("Response body :" + body.asString());
		int StatusCode = Getresponse.getStatusCode();
		log.info("Status code of Get API is : " + StatusCode);
		Assert.assertEquals(200, StatusCode);

	}

	////////////////////////////// Method 2 ::::: PUT//////////////////////////////////////////
	public void ActivationResourceProvisioningNotificationPayloadPutAPI(String url, String Token)
			throws InterruptedException, JsonProcessingException {
		String ApiURL = EnvForSalto + url;
		System.out.println("Get ApiURL : " + ApiURL);
		log.info(ApiURL);
		RestAssured.baseURI = ApiURL;
		RestAssured.useRelaxedHTTPSValidation();
		String value1 = "{\r\n   \"cctRequest\":{\r\n      \"scheduledActionDate\":\"\",\r\n      \"provisioningId\":\"91096162\",\r\n      \"acknowledgementDate\":\"\",\r\n      \"sourceCiwoId\":\"9000000046\",\r\n      \"status\":\"AK\",\r\n      \"requestedActionDate\":\"28-JUN-22\",\r\n      \"actionDate\":\"16-JUN-22\",\r\n      \"actionType\":\"INSTALL\",\r\n      \"createdDate\":\"16-JUN-22\",\r\n      \"createdBy\":\"CFRW0242\",\r\n      \"owner\":{\r\n         \"actionOwner\":\"salto\"\r\n      },\r\n      \"dateOfAction\":\"\",\r\n      \"user\":\"salto\",\r\n      \"sourceSioId\":\"9000003450\"\r\n   }\r\n}";

		Response Getresponse = RestAssured.given().header("accept", "application/hal+json")
				.header("Content-Type", "application/json").header("authorizationToken", Token).body(value1)
				.put(ApiURL);
		@SuppressWarnings("rawtypes")
		ResponseBody body = Getresponse.getBody();
		System.out.println("Response body :" + body.asString());
		int StatusCode = Getresponse.getStatusCode();
		log.info("Status code of Get API is : " + StatusCode);
		Assert.assertEquals(200, StatusCode);

	}

	public static void main(String args[]) throws InterruptedException, JsonProcessingException {
		String url = "/activation/service/access/resourceProvisioningNotification";

		SaltoUserTokenPayload usertoken = new SaltoUserTokenPayload();
		usertoken.SaltoServiceGetAPI();

		ActivationResourceProvisioningNotificationPayload activationResourceProvisioningNotification = new ActivationResourceProvisioningNotificationPayload();
		activationResourceProvisioningNotification.ActivationResourceProvisioningNotificationPayloadPostAPI(url,
				SaltoUserTokenPayload.Token);

		activationResourceProvisioningNotification.ActivationResourceProvisioningNotificationPayloadPutAPI(url,
				SaltoUserTokenPayload.Token);
	}

}
