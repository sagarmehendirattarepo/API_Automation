package Activation;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import Resource.UpdationNotificationPutPayload;
import SaltoUserToken.SaltoUserTokenPayload;
import TestBase.testbase;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import junit.framework.Assert;

public class ActivationProvisioningNotificationPayload extends testbase{
	public static final Logger log = Logger.getLogger(ActivationProvisioningNotificationPayload.class.getName());

	
//////////////////////////////Method 1 ::::: POST //////////////////////////////////////////
	public void ActivationProvisioningNotificationPostAPI(String url, String Token)
			throws InterruptedException, JsonProcessingException {
		String ApiURL = EnvForSalto + url;
		System.out.println("Get ApiURL : " + ApiURL);
		log.info(ApiURL);
		RestAssured.baseURI = ApiURL;
		RestAssured.useRelaxedHTTPSValidation();
		String value1 = "{\r\n    \"resource\": \"CA0E5DEDAE\",\r\n    \"serviceRequest\": {\r\n        \"owner\": {\r\n            \"actionOwner\": \"salto\"\r\n        },\r\n        \"provRequest\": {\r\n            \"orderType\": \"AD\",\r\n            \"orderCreatedBy\": \"\",\r\n            \"acknowledgementDate\": \"\",\r\n            \"orderBy\": \"SAX\",\r\n            \"submissionDate\": \"02-MAR-18\",\r\n            \"orderCreationDate\": \"10-JUN-20\",\r\n            \"status\": \"OP\",\r\n            \"internalOrderReference\": \"877yuexczxccsdcssdgr\",\r\n            \"modificationDate\": \"\",\r\n            \"orderReference\": \"\",\r\n            \"modifiedBy\": \"\",\r\n            \"expirationDate\": \"\"\r\n        },\r\n        \"sourceServiceId\": \"9000020734\",\r\n        \"scheduledDate\": \"24-MAY-18\",\r\n        \"user\": \"salto\",\r\n        \"sourceSioId\": \"9000003450\"\r\n    }\r\n}";

		Response Getresponse = RestAssured.given().header("accept", "application/hal+json")
				.header("Content-Type", "application/json").header("authorizationToken", Token).body(value1)
				.post(ApiURL);
		@SuppressWarnings("rawtypes")
		ResponseBody body = Getresponse.getBody();
		System.out.println("Response body :" + body.asString());
		int StatusCode = Getresponse.getStatusCode();
		log.info("Status code of Get API is : " + StatusCode);
		Assert.assertEquals(200, StatusCode);

	}
	
	
//////////////////////////////Method 2 ::::: PUT //////////////////////////////////////////
	public void ActivationProvisioningNotificationPutAPI(String url, String Token)
			throws InterruptedException, JsonProcessingException {
		String ApiURL = EnvForSalto + url;
		System.out.println("Get ApiURL : " + ApiURL);
		log.info(ApiURL);
		RestAssured.baseURI = ApiURL;
		RestAssured.useRelaxedHTTPSValidation();
		String value1 = "{\r\n   \"provisioningId\":\"91095162\",\r\n   \"serviceRequest\":{\r\n      \"owner\":{\r\n         \"actionOwner\":\"salto\"\r\n      },\r\n      \"provRequest\":{\r\n         \"orderType\":\"AD\",\r\n         \"orderCreatedBy\":\"\",\r\n         \"acknowledgementDate\":\"\",\r\n         \"orderBy\":\"SAX\",\r\n         \"submissionDate\":\"02-MAR-18\",\r\n         \"orderCreationDate\":\"10-JUN-20\",\r\n         \"status\":\"OP\",\r\n         \"internalOrderReference\":\"877yuexczxccsdcssdgr\",\r\n         \"modificationDate\":\"\",\r\n         \"orderReference\":\"\",\r\n         \"modifiedBy\":\"\",\r\n         \"expirationDate\":\"\"\r\n      },\r\n      \"sourceServiceId\":\"9000020734\",\r\n      \"scheduledDate\":\"24-MAY-18\",\r\n      \"user\":\"salto\",\r\n      \"sourceSioId\":\"9000003450\"\r\n   }\r\n}";

		Response Getresponse = RestAssured.given().header("accept", "application/hal+json")
				.header("Content-Type", "application/json").header("authorizationToken", Token).body(value1)
				.put(ApiURL);
		@SuppressWarnings("rawtypes")
		ResponseBody body = Getresponse.getBody();
		System.out.println("Response body :" + body.asString());
		int StatusCode = Getresponse.getStatusCode();
		log.info("Status code of Get API is : " + StatusCode);
		Assert.assertEquals(200, StatusCode);

	}
	

	public static void main(String args[]) throws InterruptedException, JsonProcessingException {
		String url = "/activation/service/access/provisioningNotification";

		SaltoUserTokenPayload usertoken = new SaltoUserTokenPayload();
		usertoken.SaltoServiceGetAPI();

		ActivationProvisioningNotificationPayload activationProvisioningNotification = new ActivationProvisioningNotificationPayload();
		activationProvisioningNotification.ActivationProvisioningNotificationPostAPI(url, SaltoUserTokenPayload.Token);
		
		activationProvisioningNotification.ActivationProvisioningNotificationPutAPI(url, SaltoUserTokenPayload.Token);
	}



}
