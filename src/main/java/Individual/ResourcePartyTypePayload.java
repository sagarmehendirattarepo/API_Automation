package Individual;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.core.JsonProcessingException;
import TestBase.testbase;
import UserToken.UserTokenPayload;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import junit.framework.Assert;

public class ResourcePartyTypePayload extends testbase {
	public static final Logger log = Logger.getLogger(ResourcePartyTypePayload.class.getName());

	public void ResourcePartyTypeAPI(String url, String urldata, String userToken) throws InterruptedException, JsonProcessingException {
		String ApiURL = Env + url + urldata;
		System.out.println("Get ApiURL : " + ApiURL);
		//System.out.println("Print token: " + userToken);
		RestAssured.baseURI = ApiURL;
		RestAssured.useRelaxedHTTPSValidation();
		Response Getresponse = RestAssured.given().baseUri(ApiURL).header("accept", "application/hal+json")
				.header("authorizationToken", userToken).get(ApiURL);
		
		@SuppressWarnings("rawtypes")
		ResponseBody body = Getresponse.getBody();
		log.info("Response body :" + body.asString());
		System.out.println("Response body :" + body.asString());
		int StatusCode = Getresponse.getStatusCode();
		log.info("Status code of Get API is : " + StatusCode);
		Assert.assertEquals(200, StatusCode);

}
///// This main method is used only for this class testing////
	public static void main(String args[]) throws InterruptedException, JsonProcessingException {
		String url="/resource/partyType?";
		String urldata="limit=100&offset=0&partyId=1&type=CRM_CUSTOMER";
		
		UserTokenPayload userToken2 = new UserTokenPayload();
		userToken2.ServiceGetAPI();

		ResourcePartyTypePayload resourcePartyType = new ResourcePartyTypePayload();
		resourcePartyType.ResourcePartyTypeAPI(url, urldata,UserTokenPayload.Token );
	}

}
