package Resource;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import SaltoUserToken.SaltoUserTokenPayload;
import TestBase.testbase;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import junit.framework.Assert;

public class UpdationNotificationPutPayload extends testbase {
	public static final Logger log = Logger.getLogger(UpdationNotificationPutPayload.class.getName());

	public void UpdationNotificationPutAPI(String url, String Token)
			throws InterruptedException, JsonProcessingException {
		String ApiURL = EnvForSalto + url;
		System.out.println("Get ApiURL : " + ApiURL);
		log.info(ApiURL);
		RestAssured.baseURI = ApiURL;
		RestAssured.useRelaxedHTTPSValidation();
		String value1 = "{\r\n   \"circuitConnectionRequest\":{\r\n      \"serviceType\":\"IPNET\",\r\n      \"sourceId\":\"9000000381\",\r\n      \"orderType\":\"\",\r\n      \"rfsDate\":\"\",\r\n      \"wireType\":\"\",\r\n      \"usage\":\"BACKUP\",\r\n      \"sortSequence\":null,\r\n      \"channel\":null,\r\n      \"dnic\":\"\",\r\n      \"serviceOfferName\":\"M2M - dynamic data plan - 5GB\",\r\n      \"usid\":\"GO0ENNV137\",\r\n      \"relatedPartyServiceType\":\"\",\r\n      \"reference\":\"CDC-Primary\",\r\n      \"protocol\":\"\",\r\n      \"connectionBandwidth\":0,\r\n      \"dna\":\"\",\r\n      \"analogDigital\":\"\",\r\n      \"type\":\"FWA\",\r\n      \"framing\":\"\",\r\n      \"baselineFlag\":\"\",\r\n      \"isdnType\":\"\",\r\n      \"sizeUom\":\"\",\r\n      \"dslid\":\"NLD00111\",\r\n      \"barringOption\":\"\",\r\n      \"relatedPartyLocation\":\"NL\",\r\n      \"size\":null,\r\n      \"relatedPartyId\":840,\r\n      \"orderReference\":\"\",\r\n      \"orderDate\":\"\",\r\n      \"sdm\":{\r\n         \"firstName\":\"\",\r\n         \"lastName\":\"\",\r\n         \"phone\":null,\r\n         \"userId\":\"\",\r\n         \"email\":\"\"\r\n      },\r\n      \"nsa\":\"\",\r\n      \"sourceSioId\":\"9000003450\",\r\n      \"llType\":\"\",\r\n      \"flag\":null,\r\n      \"routerUsid\":\"GO0ENNV335\",\r\n      \"sourceCiwoId\":\"9000000046\",\r\n      \"clocking\":\"\",\r\n      \"requestedBandwidth\":0,\r\n      \"coding\":\"\",\r\n      \"crc\":\"\",\r\n      \"sourceServiceId\":\"9000020734\",\r\n      \"client\":\"CUSTOMER\",\r\n      \"connectivityId\":\"\",\r\n      \"resourceServiceType\":\"\",\r\n      \"cutoverDate\":\"\",\r\n      \"address\":\"\",\r\n      \"bandwidth\":0,\r\n      \"internalReferenceId\":\"\",\r\n      \"serviceOfferGrade\":\"\",\r\n      \"status\":\"ACTIVE\",\r\n      \"carrierMonthlyAllowance\":\"5\",\r\n      \"serviceOfferCode\":\"NLD00111\",\r\n      \"urn\":\"\",\r\n      \"createdDate\":\"16-JUN-22\",\r\n      \"ldm\":{\r\n         \"firstName\":\"\",\r\n         \"lastName\":\"\",\r\n         \"phone\":\"\",\r\n         \"userId\":\"\",\r\n         \"email\":\"\"\r\n      },\r\n      \"owner\":{\r\n         \"actionOwner\":\"salto\"\r\n      },\r\n      \"createdBy\":\"CFRW0242\"\r\n   },\r\n   \"resource\":\"CA0E5DEDAE\"\r\n}";

		Response Getresponse = RestAssured.given().header("accept", "application/hal+json")
				.header("Content-Type", "application/json").header("authorizationToken", Token).body(value1)
				.put(ApiURL);
		@SuppressWarnings("rawtypes")
		ResponseBody body = Getresponse.getBody();
		System.out.println("Response body :" + body.asString());
		int StatusCode = Getresponse.getStatusCode();
		log.info("Status code of Get API is : " + StatusCode);
		Assert.assertEquals(200, StatusCode);

	}

	public static void main(String args[]) throws InterruptedException, JsonProcessingException {
		String url = "/resource/access/updationNotification";

		SaltoUserTokenPayload usertoken = new SaltoUserTokenPayload();
		usertoken.SaltoServiceGetAPI();

		UpdationNotificationPutPayload updationNotification = new UpdationNotificationPutPayload();
		updationNotification.UpdationNotificationPutAPI(url, SaltoUserTokenPayload.Token);
	}

}
