package SaltoUserToken;


import junit.framework.Assert;
import org.apache.log4j.Logger;
import com.fasterxml.jackson.core.JsonProcessingException;
import TestBase.testbase;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class SaltoUserTokenPayload extends testbase{

	public static String Token;
	public static final Logger log = Logger.getLogger(SaltoUserTokenPayload.class.getName());

	public void SaltoServiceGetAPI() throws InterruptedException, JsonProcessingException {
		String ApiURL = EnvForSalto + "/user";
		System.out.println("Get ApiURL : " + ApiURL);

		RestAssured.baseURI = ApiURL;
		RestAssured.useRelaxedHTTPSValidation();
		Response Getresponse = RestAssured.given().baseUri(ApiURL).header("accept", "*/*")
				.header("email", "xyz@gmail.com").header("key", "xyz@123").post(ApiURL);
		@SuppressWarnings("rawtypes")
		ResponseBody body = Getresponse.getBody();
		JsonPath BodyParams = Getresponse.jsonPath();
		Token = BodyParams.get("token");
		//log.info("Token : " + Token);
		//log.info("Response body :" + body.asString());
		System.out.println("Response body :" + body.asString());

		int StatusCode = Getresponse.getStatusCode();
		log.info("Status code of Post API is : " + StatusCode);
		Assert.assertEquals(200, StatusCode);

	}

	///// Main method used to test the api
	public static void main(String args[]) throws InterruptedException, JsonProcessingException {
		SaltoUserTokenPayload usertoken = new SaltoUserTokenPayload();
		usertoken.SaltoServiceGetAPI();
	}

}
