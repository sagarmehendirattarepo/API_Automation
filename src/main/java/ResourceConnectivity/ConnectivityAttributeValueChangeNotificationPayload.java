package ResourceConnectivity;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonProcessingException;

import Activation.ActivationResourceProvisioningNotificationPayload;
import SaltoUserToken.SaltoUserTokenPayload;
import TestBase.testbase;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import junit.framework.Assert;

public class ConnectivityAttributeValueChangeNotificationPayload extends testbase {
	public static final Logger log = Logger
			.getLogger(ConnectivityAttributeValueChangeNotificationPayload.class.getName());

	public void ConnectivityAttributeValueChangeNotificationPostAPI(String url, String Token)
			throws InterruptedException, JsonProcessingException {
		String ApiURL = EnvForSalto + url;
		System.out.println("Get ApiURL : " + ApiURL);
		log.info(ApiURL);
		RestAssured.baseURI = ApiURL;
		RestAssured.useRelaxedHTTPSValidation();
		String value1 = "{\r\n    \"flag\": \"N\",\r\n    \"channelNumber\": 0,\r\n    \"wireType\": \"ELECTRICAL\",\r\n    \"channel\": null,\r\n    \"referenceType\": \"CARRIER_CIRCUIT_REFERENCE\",\r\n    \"fractionalizedIndex\": \"\",\r\n    \"slotMax\": 0,\r\n    \"billing\": \"789\",\r\n    \"clocking\": \"\",\r\n    \"coding\": \"\",\r\n    \"crc\": \"\",\r\n    \"dateOfAction\": \"\",\r\n    \"analogDigital\": \"DIGITAL\",\r\n    \"type\": \"VMI\",\r\n    \"framing\": \"\",\r\n    \"resource\": \"CA0E5DEDAE\",\r\n    \"label\": \"45260\",\r\n    \"relatedParty\": \"123\",\r\n    \"jack\": \"\",\r\n    \"relatedPartyLocation\": \"\",\r\n    \"referenceComment\": \"456\",\r\n    \"owner\": {\r\n        \"actionOwner\": \"salto\"\r\n    },\r\n    \"relatedPartyId\": 11,\r\n    \"timeslot\": \"\",\r\n    \"comment\": \"456\",\r\n    \"slotMin\": 0,\r\n    \"channelizedIndex\": \"\",\r\n    \"llType\": \"\"\r\n}";

		Response Getresponse = RestAssured.given().header("accept", "application/hal+json")
				.header("Content-Type", "application/json").header("authorizationToken", Token).body(value1)
				.post(ApiURL);
		@SuppressWarnings("rawtypes")
		ResponseBody body = Getresponse.getBody();
		System.out.println("Response body :" + body.asString());
		int StatusCode = Getresponse.getStatusCode();
		log.info("Status code of Get API is : " + StatusCode);
		Assert.assertEquals(200, StatusCode);

	}

	public static void main(String args[]) throws InterruptedException, JsonProcessingException {
		String url = "/connectivity/access/attributeValueChangeNotification";

		SaltoUserTokenPayload usertoken = new SaltoUserTokenPayload();
		usertoken.SaltoServiceGetAPI();

		ConnectivityAttributeValueChangeNotificationPayload connectivityAttributeValueChangeNotification = new ConnectivityAttributeValueChangeNotificationPayload();
		connectivityAttributeValueChangeNotification.ConnectivityAttributeValueChangeNotificationPostAPI(url,
				SaltoUserTokenPayload.Token);
	}

}
